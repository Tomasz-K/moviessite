﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Services;
using MoviesSite.DataAccess.Models;
using MoviesSite.DataAccess.Repositories.Interfaces;
using System.Linq;

namespace MoviesSiteTests
{
    [TestClass]
    public class CommentsServiceTests
    {
        [TestMethod]
        public void AddComment_Null_Null()
        {
            //Arrange
            var mapperMock = new Mock<IMapper>();
            mapperMock.Setup(m => m.Map<CommentDto, Comment>(It.IsAny<CommentDto>())).Returns(() => null);

            var commentsRepositoryMock = new Mock<ICommentsRepository>();
            commentsRepositoryMock.Setup(r => r.AddComment(It.IsAny<Comment>())).Verifiable();

            var commentsService = new CommentsService(commentsRepositoryMock.Object, mapperMock.Object);

            //Act
            commentsService.AddComment(null);

            //Assert
            commentsRepositoryMock.Verify(m => m.AddComment(It.IsAny<Comment>()));
        }

        [TestMethod]
        public void AddComment_VaildData_ValidResult()
        {
            //Arrange
            CommentDto commentDto = new CommentDto();

            var mapperMock = new Mock<IMapper>();
            mapperMock.Setup(m => m.Map<CommentDto, Comment>(It.IsAny<CommentDto>())).Returns(() => new Comment());

            var commentsRepositoryMock = new Mock<ICommentsRepository>();
            commentsRepositoryMock.Setup(r => r.AddComment(It.IsAny<Comment>())).Verifiable();

            var commentsService = new CommentsService(commentsRepositoryMock.Object, mapperMock.Object);

            //Act
            commentsService.AddComment(commentDto);

            //Assert
            commentsRepositoryMock.Verify(m => m.AddComment(It.IsAny<Comment>()));
        }

        [TestMethod]
        public void GetAllCommentsByMovieId_ValidData_ValidResult()
        {
            //Arrange
            var movie = new Movie();
            movie.Comments = new List<Comment>();
            movie.Comments.Add(new Comment());
            movie.Comments.Add(new Comment());

            var commentsRepositoryMock = new Mock<ICommentsRepository>();
            commentsRepositoryMock.Setup(r => r.GetAllCommentsByMovieId(It.IsAny<int>())).Returns(() => movie.Comments);

            var mapperMock = new Mock<IMapper>();
            mapperMock.Setup(m => m.Map<Comment, CommentDto>(It.IsAny<Comment>())).Returns(() => new CommentDto());

            var commentsService = new CommentsService(commentsRepositoryMock.Object, mapperMock.Object);

            //Act
            var result = commentsService.GetAllCommentsByMovieId(It.IsAny<int>());

            //Assert
            Assert.AreEqual(2, result.ToList().Count);
        }

        public void GetAllCommentsByMovieId_MovieWithEmptyListOfComments_ResultCountEqualZero()
        {
            //Arrange
            var movie = new Movie();
            movie.Comments = new List<Comment>();

            var commentsRepositoryMock = new Mock<ICommentsRepository>();
            commentsRepositoryMock.Setup(r => r.GetAllCommentsByMovieId(It.IsAny<int>())).Returns(() => movie.Comments);

            var mapperMock = new Mock<IMapper>();
            mapperMock.Setup(m => m.Map<Comment, CommentDto>(It.IsAny<Comment>())).Returns(() => new CommentDto());

            var commentsService = new CommentsService(commentsRepositoryMock.Object, mapperMock.Object);

            //Act
            var result = commentsService.GetAllCommentsByMovieId(It.IsAny<int>());

            //Assert
            Assert.AreEqual(0, result.ToList().Count);
        }
    }
}
