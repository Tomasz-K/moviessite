﻿using System.Collections.Generic;
using MoviesSite.Business.Dto;

namespace MoviesSite.Business.Services.Interfaces
{
    public interface IUsersService
    {
        bool AddNewUser(UserDto userDto);
        void AddUserIfNoUserIsAdmin();
        bool CheckIfUserExistsInTheSystemByLogin(string login);
        void AddAdminRights(string userLogin);
        IEnumerable<UserForList> GetAllUsersForList(int loggedUserId);
        void FollowThisUser(UserToFollow userToFollow);
    }
}