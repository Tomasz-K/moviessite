﻿using System.Collections.Generic;
using MoviesSite.Business.Dto;

namespace MoviesSite.Business.Services.Interfaces
{
    public interface IRatingsService
    {
        void AddRating(RatingDto ratingDto);
        double GetAverageRateByMovieId(int id);
        IEnumerable<RatingDto> GetFollowedUsersThisMovieRatings(LoggedUserIdAndMovieId ids);
    }
}