﻿using System.Collections.Generic;
using MoviesSite.Business.Dto;

namespace MoviesSite.Business.Services.Interfaces
{
    public interface IDirectorsService
    {
        bool AddDirector(DirectorDto newDirector);
        bool CheckIfDirectorExistsInSystem(DirectorDto newDirector);
        IEnumerable<DirectorDto> GetAllDirectors();
        DirectorDto GetDirectorById(int id);
    }
}