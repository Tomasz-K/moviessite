﻿using System.Collections.Generic;
using MoviesSite.Business.Dto;

namespace MoviesSite.Business.Services.Interfaces
{
    public interface IMoviesService
    {
        IEnumerable<MovieDto> GetAllMovies();
        MovieDto GetMovieById(int id);
        bool AddMovie(MovieDto newMovie);
        IEnumerable<GenreDto> GetMovieGenres();
        void AddGenresIfDbIsEmpty();
    }
}