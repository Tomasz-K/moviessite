﻿using MoviesSite.Business.Dto;
using MoviesSite.Business.Models;

namespace MoviesSite.Business.Services.Interfaces
{
    public interface IAuthenticationService
    {
        AuthenticationResult AuthenticateUser(Credentials credentials);
    }
}