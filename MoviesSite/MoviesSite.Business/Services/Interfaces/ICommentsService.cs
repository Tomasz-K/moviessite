﻿using System.Collections.Generic;
using MoviesSite.Business.Dto;

namespace MoviesSite.Business.Services.Interfaces
{
    public interface ICommentsService
    {
        void AddComment(CommentDto commentDto);
        IEnumerable<CommentDto> GetAllCommentsByMovieId(int id);
    }
}