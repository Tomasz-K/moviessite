﻿using System;
using AutoMapper;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Services.Interfaces;
using MoviesSite.DataAccess.Models;
using MoviesSite.DataAccess.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace MoviesSite.Business.Services
{
    public class MoviesService : IMoviesService
    {
        private readonly IMoviesRepository _moviesRepository;
        private readonly IMapper _mapper;

        public MoviesService(IMoviesRepository moviesRepository, IMapper mapper)
        {
            _moviesRepository = moviesRepository;
            _mapper = mapper;
        }

        public IEnumerable<MovieDto> GetAllMovies()
        {
            var movieEntitiesList = _moviesRepository.GetAllMovies();
            var moviesList = new List<MovieDto>();

            foreach (var movie in movieEntitiesList)
            {
                var temp = _mapper.Map<Movie, MovieDto>(movie);
                moviesList.Add(temp);
            }

            return moviesList;
        }

        public MovieDto GetMovieById(int id)
        {
            var movieEntity = _moviesRepository.GetMovieById(id);

            return _mapper.Map<Movie, MovieDto>(movieEntity);
        }

        public bool AddMovie(MovieDto newMovie)
        {
            if (CheckIfMovieExistsInSystem(newMovie))
            {
                throw new Exception("Movie already in the system!");
            }

            var newMovieEntity = _mapper.Map<MovieDto, Movie>(newMovie);

            return _moviesRepository.AddMovie(newMovieEntity);
        }

        public bool CheckIfMovieExistsInSystem(MovieDto newMovie)
        {
            var movie = _moviesRepository.GetMovieByName(_mapper.Map<MovieDto, Movie>(newMovie));

            if (movie == null)
            {
                return false;
            }

            return true;
        }

        public IEnumerable<GenreDto> GetMovieGenres()
        {
            var genreEntitiesList = _moviesRepository.GetMovieGenres();
            var genresList = new List<GenreDto>();

            foreach (var genre in genreEntitiesList)
            {
                var temp = _mapper.Map<Genre, GenreDto>(genre);
                genresList.Add(temp);
            }

            return genresList;
        }

        public void AddGenresIfDbIsEmpty()
        {
            if (!_moviesRepository.GetMovieGenres().Any())
            {
                _moviesRepository.AddGenresIfDatabaseEmpty();
            }
        }
    }
}
