﻿using AutoMapper;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Services.Interfaces;
using MoviesSite.DataAccess.Models;
using MoviesSite.DataAccess.Repositories.Interfaces;
using System;
using System.Collections.Generic;

namespace MoviesSite.Business.Services
{
    public class RatingsService : IRatingsService
    {
        private readonly IRatingsRepository _ratingsRepository;
        private readonly IMapper _mapper;
        private readonly IUsersRepository _usersRepository;

        public RatingsService(IRatingsRepository ratingsRepository, IMapper mapper, IUsersRepository usersRepository)
        {
            _ratingsRepository = ratingsRepository;
            _mapper = mapper;
            _usersRepository = usersRepository;
        }
        

        public void AddRating(RatingDto ratingDto)
        {
            var rating = _mapper.Map<RatingDto, Rating>(ratingDto);
            _ratingsRepository.AddRating(rating);
        }

        public IEnumerable<RatingDto> GetFollowedUsersThisMovieRatings(LoggedUserIdAndMovieId ids)
        {
            var allRatingsDtoList = new List<RatingDto>();

            foreach (var user in _usersRepository.GetAllFollowedUsers(ids.LoggedUserId))
            {
                var userDto = _mapper.Map<User, UserDto>(user);
                var rating = _ratingsRepository.GetRatingByMovieIdAndLogin(ids.MovieId, userDto.Login);
                var ratingDto = _mapper.Map<Rating, RatingDto>(rating);
                if(rating != null) 
                    allRatingsDtoList.Add(ratingDto);
            }

            return allRatingsDtoList;
        }

        public double GetAverageRateByMovieId(int id)
        {
            var ratingsList = _ratingsRepository.GetAllRatingsByMovieId(id);
            
            var ratingsDtoList = new List<RatingDto>();

            foreach (var rate in ratingsList)
            {
                var ratingDto = _mapper.Map<Rating, RatingDto>(rate);
                ratingsDtoList.Add(ratingDto);
            }

            return Average(ratingsDtoList);
        }

        private double Average(List<RatingDto> ratingsDtoList)
        {
            var sum = 0;
            double average = 0;

            for (int i = 0; i < ratingsDtoList.Count; i++)
            {
                sum = sum + ratingsDtoList[i].Rate;
            }

            average = (double)sum / (double)ratingsDtoList.Count;

            return Math.Round(average, 2);
        }
    }
}
