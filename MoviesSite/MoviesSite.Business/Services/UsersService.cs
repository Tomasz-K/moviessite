﻿using AutoMapper;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Services.Interfaces;
using MoviesSite.DataAccess.Models;
using MoviesSite.DataAccess.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MoviesSite.Business.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUsersRepository _usersRepository;
        private readonly IMapper _mapper;

        public UsersService(IUsersRepository usersRepository, IMapper mapper)
        {
            _usersRepository = usersRepository;
            _mapper = mapper;
        }

        public void AddUserIfNoUserIsAdmin()
        {
            if (_usersRepository.CheckIfAnyUserIsAdmin() == false)
            {
                _usersRepository.AddUser();
            }
        }

        public bool CheckIfUserExistsInTheSystemByLogin(string login)
        {
            var user = _usersRepository.GetUserByLogin(login);

            if (user == null)
            {
                return false;
            }

            return true;
        }
        
        public IEnumerable<UserForList> GetAllUsersForList(int loggedUserId)
        {
            var usersForList = new List<UserForList>();
            var loggedUser = _usersRepository.GetUserById(loggedUserId);

            foreach (var user in _usersRepository.GetAll())
            {
                var userForList = new UserForList
                {
                    UserLogin = user.Login,
                    IsAdmin = user.IsAdmin,
                    IsFollowed = _usersRepository.IsFollowed(loggedUserId, user.Id),

                };
                usersForList.Add(userForList);
                usersForList.Remove(usersForList.SingleOrDefault(u => u.UserLogin == "DefaultAdmin"));
                usersForList.Remove(usersForList.SingleOrDefault(u => u.UserLogin == loggedUser.Login));
            }

            return usersForList;
        }

        public void FollowThisUser(UserToFollow userToFollow)
        {
            _usersRepository.FollowThisUser(userToFollow.UserToFollowLogin, userToFollow.LoggedUserId);
        }

        public void AddAdminRights(string userLogin)
        {
            var flagAsAdmin = true;
            _usersRepository.AddAdminRights(userLogin, flagAsAdmin);
        }

        

        public bool AddNewUser(UserDto userDto)
        {
            if (CheckIfUserExistsInTheSystemByLogin(userDto.Login))
            {
                throw new Exception("User with the same login exists!");
            }

            var user = _mapper.Map<UserDto, User>(userDto);

            return _usersRepository.AddNewUser(user);
        }
    }
}
