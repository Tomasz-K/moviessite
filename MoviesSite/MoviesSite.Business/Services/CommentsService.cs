﻿using AutoMapper;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Services.Interfaces;
using MoviesSite.DataAccess.Models;
using MoviesSite.DataAccess.Repositories.Interfaces;
using System.Collections.Generic;

namespace MoviesSite.Business.Services
{
    public class CommentsService : ICommentsService
    {
        private readonly ICommentsRepository _commentsRepository;
        private readonly IMapper _mapper;

        public CommentsService(ICommentsRepository commentsRepository, IMapper mapper)
        {
            _mapper = mapper;
            _commentsRepository = commentsRepository;
        }

        public void AddComment(CommentDto commentDto)
        {
            var comment = _mapper.Map<CommentDto, Comment>(commentDto);
            _commentsRepository.AddComment(comment);
        }

        public IEnumerable<CommentDto> GetAllCommentsByMovieId(int id)
        {
            var commentsList = _commentsRepository.GetAllCommentsByMovieId(id);
            var commentsDtoList = new List<CommentDto>();

            foreach (var comment in commentsList)
            {
                var commentDto = _mapper.Map<Comment, CommentDto>(comment);
                commentsDtoList.Add(commentDto);
            }

            return commentsDtoList;
        }
    }
}
