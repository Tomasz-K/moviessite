﻿using System;
using System.Collections.Generic;
using AutoMapper;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Services.Interfaces;
using MoviesSite.DataAccess.Models;
using MoviesSite.DataAccess.Repositories.Interfaces;

namespace MoviesSite.Business.Services
{
    public class DirectorsService : IDirectorsService
    {
        private readonly IDirectorsRepository _directorsRepository;
        private readonly IMapper _mapper;

        public DirectorsService(IDirectorsRepository directorsRepository, IMapper mapper)
        {
            _directorsRepository = directorsRepository;
            _mapper = mapper;
        }

        public bool AddDirector(DirectorDto newDirector)
        {
            if (CheckIfDirectorExistsInSystem(newDirector))
            {
                throw new Exception("Director already in the system!");
            }

            var newDirectorEntity = _mapper.Map<DirectorDto, Director>(newDirector);

            return _directorsRepository.AddDirector(newDirectorEntity);
        }

        public bool CheckIfDirectorExistsInSystem(DirectorDto newDirector)
        {
            var director = _directorsRepository.GetDirectorByPersonalData(_mapper.Map<DirectorDto, Director>(newDirector));

            if (director == null)
            {
                return false;
            }

            return true;
        }

        public IEnumerable<DirectorDto> GetAllDirectors()
        {
            var directorEntityList = _directorsRepository.GetAllDirectors();
            var directorsList = new List<DirectorDto>();

            foreach (var director in directorEntityList)
            {
                var temp = _mapper.Map<Director, DirectorDto>(director);
                directorsList.Add(temp);
            }

            return directorsList;
        }

        public DirectorDto GetDirectorById(int id)
        {
            var directorEntity = _directorsRepository.GetDirectorbyId(id);

            return _mapper.Map<Director, DirectorDto>(directorEntity);
        }
    }
}
