﻿using AutoMapper;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Models;
using MoviesSite.Business.Services.Interfaces;
using MoviesSite.DataAccess.Models;
using MoviesSite.DataAccess.Repositories.Interfaces;

namespace MoviesSite.Business.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUsersRepository _usersRepository;
        private readonly IMapper _mapper;

        public AuthenticationService(IUsersRepository usersRepository, IMapper mapper)
        {
            _usersRepository = usersRepository;
            _mapper = mapper;
        }

        public AuthenticationResult AuthenticateUser(Credentials credentials)
        {
            var isAuthenticated = _usersRepository.PasswordChecker(credentials.Login, credentials.Password);

            if (!isAuthenticated)
                return new AuthenticationResult
                {
                    IsAuthenticated = false,
                };

            var userDto = _mapper.Map < User, UserDto>(_usersRepository.GetUserByLogin(credentials.Login));

            return new AuthenticationResult
            {
                UserId = userDto.Id,
                UserLogin = userDto.Login,
                IsAuthenticated = true,
                IsAdmin = userDto.IsAdmin
            };
        }
    }
}