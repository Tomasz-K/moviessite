﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesSite.Business.Dto
{
    public class UserForList
    {
        public string UserLogin { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsFollowed { get; set; }
    }
}
