﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesSite.Business.Dto
{
    public class UserToFollow
    {
        public int LoggedUserId { get; set; }
        public string UserToFollowLogin { get; set; }
    }
}
