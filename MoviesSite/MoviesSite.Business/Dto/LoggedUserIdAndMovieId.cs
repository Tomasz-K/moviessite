﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesSite.Business.Dto
{
    public class LoggedUserIdAndMovieId
    {
        public int LoggedUserId { get; set; }
        public int MovieId { get; set; }
    }
}

