﻿namespace MoviesSite.Business.Dto
{
    public class CommentDto
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public string Login { get; set; }
        public string Content { get; set; }
    }
}