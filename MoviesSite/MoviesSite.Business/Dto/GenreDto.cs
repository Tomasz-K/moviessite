﻿using System.Collections.Generic;

namespace MoviesSite.Business.Dto
{
    public class GenreDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<MovieDto> Movies { get; set; }
    }
}
