﻿using System;
using System.Collections.Generic;

namespace MoviesSite.Business.Dto
{
    public class MovieDto
    {
       public int Id { get; set; }
       public string Title { get; set; }
       public DateTime ProductionYear { get; set; }
       public string Description { get; set; }
       public int GenreId { get; set; }
       public int DirectorId { get; set; }
       public List<RatingDto> UsersRating{ get;  set; }
       public List<CommentDto> Comments { get; set; }
    }
}
