﻿namespace MoviesSite.Business.Dto
{
    public class AuthenticationResult
    {
        public int UserId { get; set; }
        public string UserLogin { get; set; }
        public bool IsAuthenticated { get; set; }
        public bool IsAdmin { get; set; }
    }
}
