﻿using System.Collections.Generic;

namespace MoviesSite.Business.Dto
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public List<UserDto> FollowedUsers { get; set; }
    }
}
