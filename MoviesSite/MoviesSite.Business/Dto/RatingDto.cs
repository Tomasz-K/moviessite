﻿namespace MoviesSite.Business.Dto
{
    public class RatingDto
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public string Login { get; set; }
        public int Rate { get; set; }
    }
}