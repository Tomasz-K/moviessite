﻿using System;
using System.Collections.Generic;

namespace MoviesSite.Business.Dto
{
    public class DirectorDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime Birthdate { get; set; }
        public List<MovieDto> Movies { get; set; }
    }
}
