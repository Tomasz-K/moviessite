﻿using AutoMapper;
using MoviesSite.Business.Mapping;
using MoviesSite.Business.Services;
using MoviesSite.Business.Services.Interfaces;
using MoviesSite.DataAccess.Modules;
using Ninject.Modules;

namespace MoviesSite.Business.Modules
{
    public  class BusinessModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMapper>().ToConstant(new AutomapperConfigProvider().GetConfig().CreateMapper());

            this.Kernel.Load(new[] { new DataAccessModule() });
            this.Kernel.Bind<IUsersService>().To<UsersService>();
            this.Kernel.Bind<IAuthenticationService>().To<AuthenticationService>();
			Bind<IMoviesService>().To<MoviesService>();
            Bind<IDirectorsService>().To<DirectorsService>();
            Bind<ICommentsService>().To<CommentsService>();
            Bind<IRatingsService>().To<RatingsService>();
        }
    }
}