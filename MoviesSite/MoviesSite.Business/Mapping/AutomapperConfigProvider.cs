﻿using AutoMapper;
using MoviesSite.Business.Dto;
using MoviesSite.DataAccess.Models;

namespace MoviesSite.Business.Mapping
{
    public class AutomapperConfigProvider
    {
        public MapperConfiguration GetConfig()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Comment, CommentDto>();
                cfg.CreateMap<CommentDto, Comment>();
                cfg.CreateMap<Director, DirectorDto>().MaxDepth(1);
                cfg.CreateMap<DirectorDto, Director>();
                cfg.CreateMap<Genre, GenreDto>();
                cfg.CreateMap<GenreDto, Genre>();
                cfg.CreateMap<Movie, MovieDto>().MaxDepth(1);
                cfg.CreateMap<MovieDto, Movie>();
                cfg.CreateMap<Rating, RatingDto>();
                cfg.CreateMap<RatingDto, Rating>();
                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<UserDto, User>();
            });

            return config;
        }
    }
}
