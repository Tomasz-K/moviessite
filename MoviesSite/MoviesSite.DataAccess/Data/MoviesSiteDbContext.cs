﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using MoviesSite.DataAccess.Models;

namespace MoviesSite.DataAccess.Data
{
    public class MoviesSiteDbContext : DbContext
    {
        public MoviesSiteDbContext() : base(GetConnectionString())
        {
            Database.SetInitializer<MoviesSiteDbContext>(new AdminDbInitializer<MoviesSiteDbContext>());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Director> Directors { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Genre> Genres { get; set; }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MoviesSiteDbContext"].ConnectionString;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasMany(c => c.FollowedUsers).WithMany(c => c.FollowingUsers)
                .Map(m =>
                {
                    m.MapLeftKey("FollowingUserId");
                    m.MapRightKey("FollowedUserId");
                    m.ToTable("UserUsers");
                });
        }

        private class AdminDbInitializer<T> : CreateDatabaseIfNotExists<MoviesSiteDbContext>
        {
            protected override void Seed(MoviesSiteDbContext dbContext)
            {
                var user = new User()
                {
                    Login = "DefaultAdmin",
                    Password = "123456",
                    IsAdmin = true
                };

                dbContext.Users.Add(user);

                var genres = new List<Genre>()
                    {
                        new Genre()
                        {
                          Name  = "Scifi"
                        },
                        new Genre()
                        {
                            Name  = "Action"
                        },
                        new Genre()
                        {
                            Name  = "Thriller"
                        },
                        new Genre()
                        {
                            Name  = "Drama"
                        },
                    };

                foreach (var genre in genres)
                {
                    dbContext.Genres.Add(genre);
                }

                base.Seed(dbContext);
            }
        }
    }
}
