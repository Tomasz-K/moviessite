﻿using System;
using System.Collections.Generic;

namespace MoviesSite.DataAccess.Models
{
    public class Movie
    {
       public int Id { get; set; }
       public string Title { get; set; }
       public DateTime ProductionYear { get; set; }
       public string Description { get; set; }
       public int GenreId { get; set; }
       public int DirectorId { get; set; }
       public List<Rating> UsersRating{ get;  set; }
       public List<Comment> Comments { get; set; }
    }
}
