﻿using System.Collections.Generic;

namespace MoviesSite.DataAccess.Models
{
    public class User
    {
        public User()
        {
            FollowedUsers = new HashSet<User>();
            FollowingUsers = new HashSet<User>();
        }

        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public ICollection<User> FollowedUsers { get; set; }
        public ICollection<User> FollowingUsers { get; set; }
    }
}
