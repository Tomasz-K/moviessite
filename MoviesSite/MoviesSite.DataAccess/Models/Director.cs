﻿using System;
using System.Collections.Generic;

namespace MoviesSite.DataAccess.Models
{
    public class Director
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime Birthdate { get; set; }
        public List<Movie> Movies { get; set; }
    }
}
