﻿using MoviesSite.DataAccess.Repositories;
using MoviesSite.DataAccess.Repositories.Interfaces;
using Ninject.Modules;

namespace MoviesSite.DataAccess.Modules
{
    public class DataAccessModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMoviesRepository>().To<MoviesRepository>();
            Bind<IUsersRepository>().To<UsersRepository>();
            Bind<IDirectorsRepository>().To<DirectorsRepository>();
            Bind<ICommentsRepository>().To<CommentsRepository>();
            Bind<IRatingsRepository>().To<RatingsRepository>();
        }
    }
}
