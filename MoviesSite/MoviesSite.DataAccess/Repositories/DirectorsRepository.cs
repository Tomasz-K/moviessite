﻿using System.Collections.Generic;
using System.Linq;
using MoviesSite.DataAccess.Data;
using MoviesSite.DataAccess.Models;
using MoviesSite.DataAccess.Repositories.Interfaces;

namespace MoviesSite.DataAccess.Repositories
{
    public class DirectorsRepository : IDirectorsRepository
    {
        public bool AddDirector(Director newDirector)
        {
            Director addedDirector = null;

            using (var dbContext = new MoviesSiteDbContext())
            {
                addedDirector = dbContext.Directors.Add(newDirector);
                dbContext.SaveChanges();
            }

            return addedDirector != null;
        }

        public Director GetDirectorByPersonalData(Director newDirector)
        {
            Director foundDirector = null;

            using (var dbContext = new MoviesSiteDbContext())
            {
                foundDirector = dbContext.Directors.SingleOrDefault(d => d.Name == newDirector.Name && d.LastName == newDirector.LastName);
            }

            return foundDirector;
        }

        public IEnumerable<Director> GetAllDirectors()
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                return dbContext.Directors.ToList();
            }
        }

        public Director GetDirectorbyId(int id)
        {
            Director director = null;

            using (var dbContext = new MoviesSiteDbContext())
            {
                director = dbContext.Directors.SingleOrDefault(d => d.Id == id);
            }

            return director;
        }
    }
}
