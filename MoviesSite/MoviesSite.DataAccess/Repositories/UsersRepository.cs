﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MoviesSite.DataAccess.Data;
using MoviesSite.DataAccess.Models;
using MoviesSite.DataAccess.Repositories.Interfaces;

namespace MoviesSite.DataAccess.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        public void AddUser()
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                dbContext.Users.Add(new User()
                {
                    Login = "Admin",
                    Password = "123",
                    IsAdmin = true
                });
                dbContext.SaveChanges();
            }
        }

        public bool PasswordChecker(string credentialsLogin, string credentialsPassword)
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                var user = dbContext.Users
                    .SingleOrDefault(u => u.Login == credentialsLogin);

                return user != null && user.Password == credentialsPassword;
            }
        }

        public User GetUserByLogin(string login)
        {
            User user = null;

            using (var dbContext = new MoviesSiteDbContext())
            {
                user = dbContext.Users.SingleOrDefault(u => u.Login == login);
            }

            return user;


        }

        public User GetUserById(int id)
        {
            User user = null;

            using (var dbContext = new MoviesSiteDbContext())
            {
                user = dbContext.Users.SingleOrDefault(u => u.Id == id);
            }

            return user;
        }

        public IEnumerable<User> GetAllFollowedUsers(int loggedUserId)
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                var list =  dbContext.Users.Include(u => u.FollowedUsers).SingleOrDefault(u => u.Id == loggedUserId).FollowedUsers.ToList();
                return list;
            }
            
        }

        public bool AddNewUser(User user)
        {
            User addedUser = null;

            using (var dbContext = new MoviesSiteDbContext())
            {
                addedUser = dbContext.Users.Add(user);
                dbContext.SaveChanges();
            }

            return addedUser != null;
        }

        public bool CheckIfAnyUserIsAdmin()
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                var foundUser = dbContext.Users.Any(u => u.IsAdmin == true);

                return foundUser;
            }
        }

        public IEnumerable<User> GetAll()
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                return dbContext.Users.ToList();
            }
        }

        public void AddAdminRights(string userLogin, bool flagAsAdmin)
        {
            var user = GetUserByLogin(userLogin);
            user.IsAdmin = flagAsAdmin;

            using (var dbContext = new MoviesSiteDbContext())
            {
                if (!dbContext.Users.Local.Contains(user))
                {
                    dbContext.Users.Attach(user);
                    dbContext.Entry(user)
                        .Property(s => s.IsAdmin)
                        .IsModified = true;

                }
                dbContext.SaveChanges();
            }
        }

        public bool IsFollowed(int loggedUserId, int currentUserId)
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
               var isFollowed = dbContext.Users.Include(user => user.FollowedUsers).SingleOrDefault(u => u.Id == loggedUserId).FollowedUsers.Any(fU => fU.Id == currentUserId);

                return isFollowed;
            }
        }

        public void FollowThisUser(string userToFollowLogin, int loggedUserId)
        {
            var userToFollow = GetUserByLogin(userToFollowLogin);

            using (var dbContext = new MoviesSiteDbContext())
            {
                if (!dbContext.Users.Local.Contains(userToFollow))
                {
                    dbContext.Users
                        .Attach(userToFollow);

                    dbContext.Entry(userToFollow)
                        .State = EntityState.Modified;
                }

                dbContext.Users.Include(u => u.FollowedUsers).SingleOrDefault(u => u.Id == loggedUserId).FollowedUsers.Add(userToFollow);

                dbContext.SaveChanges();
            }
            
        }
    }
}
