﻿using System.Collections.Generic;
using MoviesSite.DataAccess.Models;

namespace MoviesSite.DataAccess.Repositories.Interfaces
{
    public interface IDirectorsRepository
    {
        bool AddDirector(Director newDirector);
        Director GetDirectorByPersonalData(Director newDirector);
        IEnumerable<Director> GetAllDirectors();
        Director GetDirectorbyId(int id);
    }
}