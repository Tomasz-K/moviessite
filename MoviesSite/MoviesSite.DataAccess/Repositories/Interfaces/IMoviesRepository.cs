﻿using System.Collections.Generic;
using MoviesSite.DataAccess.Models;

namespace MoviesSite.DataAccess.Repositories.Interfaces
{
    public interface IMoviesRepository
    {
        IEnumerable<Movie> GetAllMovies();
        Movie GetMovieById(int id);
        bool AddMovie(Movie newMovieEntity);
        Movie GetMovieByName(Movie newMovie);
        IEnumerable<Genre> GetMovieGenres();
        void AddGenresIfDatabaseEmpty();
    }
}