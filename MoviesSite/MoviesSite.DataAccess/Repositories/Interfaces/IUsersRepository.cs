﻿using System.Collections.Generic;
using MoviesSite.DataAccess.Models;

namespace MoviesSite.DataAccess.Repositories.Interfaces
{
    public interface IUsersRepository
    {
        bool AddNewUser(User user);
        void AddUser();
        bool PasswordChecker(string credentialsLogin, string credentialsPassword);
        User GetUserByLogin(string login);
        bool CheckIfAnyUserIsAdmin();
        IEnumerable<User> GetAll();
        void AddAdminRights(string userLogin, bool flagAsAdmin);
        bool IsFollowed(int loggedUserId, int currentUserId);
        void FollowThisUser(string userToFollowLogin, int loggedUserId);
        User GetUserById(int id);
        IEnumerable<User> GetAllFollowedUsers(int loggedUserId);
    }
}