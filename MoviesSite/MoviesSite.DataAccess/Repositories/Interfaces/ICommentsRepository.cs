﻿using System.Collections.Generic;
using MoviesSite.DataAccess.Models;

namespace MoviesSite.DataAccess.Repositories.Interfaces
{
    public interface ICommentsRepository
    {
        void AddComment(Comment comment);
        IEnumerable<Comment> GetAllCommentsByMovieId(int id);
    }
}