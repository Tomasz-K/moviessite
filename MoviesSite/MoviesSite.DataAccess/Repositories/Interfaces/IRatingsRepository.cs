﻿using System.Collections.Generic;
using MoviesSite.DataAccess.Models;

namespace MoviesSite.DataAccess.Repositories.Interfaces
{
    public interface IRatingsRepository
    {
        void AddRating(Rating rating);
        IEnumerable<Rating> GetAllRatingsByMovieId(int id);
        Rating GetRatingByMovieIdAndLogin(int id, string login);
    }
}