﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoviesSite.DataAccess.Data;
using MoviesSite.DataAccess.Models;
using MoviesSite.DataAccess.Repositories.Interfaces;

namespace MoviesSite.DataAccess.Repositories
{
    public class CommentsRepository : ICommentsRepository
    {

        public void AddComment(Comment comment)
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
            
                dbContext.Comments.Add(comment);
                dbContext.SaveChanges();
            }
        }

        public IEnumerable<Comment> GetAllCommentsByMovieId(int id)
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                return dbContext.Movies
                    .Include(c => c.Comments)
                    .SingleOrDefault(m => m.Id == id).Comments.ToList();
            }
        }
    }
}
