﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoviesSite.DataAccess.Data;
using MoviesSite.DataAccess.Models;
using MoviesSite.DataAccess.Repositories.Interfaces;

namespace MoviesSite.DataAccess.Repositories
{
    public class RatingsRepository : IRatingsRepository
    {

        public void AddRating(Rating rating)
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                var ratingFromDb =
                    dbContext.Ratings.SingleOrDefault(r => r.Login == rating.Login && r.MovieId == rating.MovieId);
                if (ratingFromDb != null)
                {
                    ratingFromDb.Rate = rating.Rate;
                    dbContext.Ratings.Attach(ratingFromDb);
                    dbContext.Entry(ratingFromDb)
                        .Property(s => s.Rate)
                        .IsModified = true;
                }
                else
                {
                dbContext.Ratings.Add(rating);
                }
                dbContext.SaveChanges();
            }

        }
        public IEnumerable<Rating> GetAllRatingsByMovieId(int id)
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                return dbContext.Movies
                    .Include(x => x.UsersRating)
                    .SingleOrDefault(m => m.Id == id).UsersRating.ToList();
            }
        }

        public Rating GetRatingByMovieIdAndLogin(int id, string login)
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                return dbContext.Movies
                    .Include(x => x.UsersRating)
                    .SingleOrDefault(m => m.Id == id)?.UsersRating.SingleOrDefault(r => r.Login == login);
            }
        }
    }
}
