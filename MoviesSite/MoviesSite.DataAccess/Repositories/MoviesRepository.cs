﻿using MoviesSite.DataAccess.Data;
using MoviesSite.DataAccess.Models;
using System.Collections.Generic;
using System.Linq;
using MoviesSite.DataAccess.Repositories.Interfaces;
using System.Data.Entity;

namespace MoviesSite.DataAccess.Repositories
{
    public class MoviesRepository : IMoviesRepository
    {
        public IEnumerable<Movie> GetAllMovies()
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                return dbContext.Movies.ToList();
            }
        }

        public Movie GetMovieById(int id)
        {
            Movie movie = null;

            using (var dbContext = new MoviesSiteDbContext())
            {
                movie = dbContext.Movies.Include(c => c.Comments).Include(u => u.UsersRating)
                    .Single(m => m.Id == id);
            }

            return movie;
        }

        public bool AddMovie(Movie newMovie)
        {
            Movie addedMovie = null;

            using (var dbContext = new MoviesSiteDbContext())
            {
                addedMovie = dbContext.Movies.Add(newMovie);
                dbContext.SaveChanges();
            }

            return addedMovie != null;
        }

        public Movie GetMovieByName(Movie newMovie)
        {
            Movie foundMovie = null;

            using (var dbContext = new MoviesSiteDbContext())
            {
                foundMovie = dbContext.Movies.SingleOrDefault(m => m.Title == newMovie.Title);
            }

            return foundMovie;
        }

        public IEnumerable<Genre> GetMovieGenres()
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                return dbContext.Genres.ToList();
            }
        }

        public void AddGenresIfDatabaseEmpty()
        {
            using (var dbContext = new MoviesSiteDbContext())
            {
                var genres = new List<Genre>()
                {
                    new Genre()
                    {
                        Name  = "Scifi"
                    },
                    new Genre()
                    {
                        Name  = "Action"
                    },
                    new Genre()
                    {
                        Name  = "Thriller"
                    },
                    new Genre()
                    {
                        Name  = "Drama"
                    },
                };

                foreach (var genre in genres)
                {
                    dbContext.Genres.Add(genre);
                }
                dbContext.SaveChanges();
            }
        }
    }
}
