﻿using MoviesSite.WebApi.Bootstrap;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace MoviesSite.WebApi.AppStart
{
    public class StartUp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.EnableCors(new EnableCorsAttribute("*", "*", "*", "X-Custom-Header"));

            config.Routes.MapHttpRoute(
                name: "ControllerOnly",
                routeTemplate: "movies-api/{controller}"
            );

            config.Routes.MapHttpRoute(
                name: "ControllerAndId",
                routeTemplate: "movies-api/{controller}/{id}",
                defaults: new
                {
                    id = RouteParameter.Optional,
                }
            );

            //summary
            //below using custom exception handler.
            //it catches all not handled exceptions in the web-api.
            //here used for catching an exception while registering.
            //if user tries to register login already existing in the database, 
            //exception is thrown and caught by MyExceptionHandler();
            //error from MyExceptionHandler is worked on in backen
            config.Services.Replace(typeof(IExceptionHandler), new MyExceptionHandler());

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            appBuilder.UseNinjectMiddleware(NinjectBootstrap.GetKernel).UseNinjectWebApi(config);
        }
    }

    public class MyExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            context.Result = new InternalServerErrorResult(context.Request);
        }
    }
}
