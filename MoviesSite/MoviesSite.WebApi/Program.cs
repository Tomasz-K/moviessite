﻿using Microsoft.Owin.Hosting;
using MoviesSite.WebApi.AppStart;
using System;
using System.Net.Http;
using MoviesSite.Business.Services;
using MoviesSite.Business.Services.Interfaces;

namespace MoviesSite.WebApi
{
    class Program
    {
        static void Main()
        {
            const string baseAddress = "http://localhost:9000/";

            using (WebApp.Start<StartUp>(baseAddress))
            {
                var httpClient = new HttpClient();

                var response = httpClient.GetAsync(baseAddress + "movies-api/status").Result;

                Console.WriteLine($"{response}\n" +
                                  $"{response.Content.ReadAsStringAsync().Result}\n" +
                                  $"Status code: {response.StatusCode}");

               
                Console.ReadLine();
            }
        }
    }
}
