﻿using MoviesSite.Business.Modules;
using Ninject;

namespace MoviesSite.WebApi.Bootstrap
{
    public class NinjectBootstrap
    {
        public static IKernel GetKernel()
        {
            var kernel = new StandardKernel();

            kernel.Load(new [] {new BusinessModule()});
            return kernel;
        }

    }
}
