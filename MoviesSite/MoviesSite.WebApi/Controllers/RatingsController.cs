﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Services.Interfaces;

namespace MoviesSite.WebApi.Controllers
{
    public class RatingsController : ApiController
    {
        private readonly IRatingsService _ratingsService;

        public RatingsController(IRatingsService ratingsService)
        {
            _ratingsService = ratingsService;
        }

        [HttpPost]
        public void AddRating(RatingDto ratingDto)
        {
            _ratingsService.AddRating(ratingDto);
        }

        [HttpGet]
        public double AverageRate(int id)
        {
            return _ratingsService.GetAverageRateByMovieId(id);
        }
    }
}
