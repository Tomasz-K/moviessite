﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Services.Interfaces;

namespace MoviesSite.WebApi.Controllers
{
    public class GenreController : ApiController
    {
        private readonly IMoviesService _moviesService;

        public GenreController(IMoviesService moviesService)
        {
            _moviesService = moviesService;
        }

        [HttpGet]
        public IEnumerable<GenreDto> GetGenres()
        {
            return _moviesService.GetMovieGenres();
        }

        [HttpPost]
        public void AddGenre(GenreDto newGenre)
        {   //
            //TO DO: adding custom genre could be added here
            //
        }
    }
}
