﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Models;
using MoviesSite.Business.Services;
using MoviesSite.Business.Services.Interfaces;

namespace MoviesSite.WebApi.Controllers
{
    public class AuthenticationController : ApiController
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost]
        public AuthenticationResult AuthenticateUser(Credentials credentials)
        {
            return _authenticationService.AuthenticateUser(credentials);
        }
    }
}
