﻿using System.Collections.Generic;
using System.Web.Http;
using MoviesSite.Business.Services.Interfaces;
using MoviesSite.Business.Dto;

namespace MoviesSite.WebApi.Controllers
{
    public class DirectorController : ApiController
    {
        private readonly IDirectorsService _directorsService;

        public DirectorController(IDirectorsService directorsService)
        {
            _directorsService = directorsService;
        }

        [HttpPost]
        public void AddDirector(DirectorDto directorDto)
        {
            _directorsService.AddDirector(directorDto);
        }

        [HttpGet]
        public IEnumerable<DirectorDto> GetAllDirectors()
        {
            return _directorsService.GetAllDirectors();
        }

        [HttpGet]
        public DirectorDto GetDirectorById(int id)
        {
            return _directorsService.GetDirectorById(id);
        }
    }
}
