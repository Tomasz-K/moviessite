﻿using System;
using System.Web.Http;
using MoviesSite.Business.Services.Interfaces;

namespace MoviesSite.WebApi.Controllers
{
    public class StatusController : ApiController
    {
        private readonly IUsersService _usersService;
        private readonly IMoviesService _moviesService;
        
        public StatusController(IUsersService usersService, IMoviesService moviesService)
        {
            _usersService = usersService;
            _moviesService = moviesService;
        }

        public string Get()
        {
            _usersService.AddUserIfNoUserIsAdmin();
            _moviesService.AddGenresIfDbIsEmpty();
            return "Api running";
        }
    }
}
