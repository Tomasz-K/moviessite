﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Services;
using MoviesSite.Business.Services.Interfaces;

namespace MoviesSite.WebApi.Controllers
{
    public class UserRelationsController : ApiController
    {
        private readonly IUsersService _usersService;

        public UserRelationsController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpPost]
        public void FollowThisUser(UserToFollow userToFollow)
        {
             _usersService.FollowThisUser(userToFollow);
        }
    }
}
