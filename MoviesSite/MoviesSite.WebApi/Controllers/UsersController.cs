﻿using MoviesSite.Business.Dto;
using MoviesSite.Business.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace MoviesSite.WebApi.Controllers
{
    public class UsersController : ApiController
    {
        private readonly IUsersService _usersService;
        
        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }
        
        [HttpGet]
        public IEnumerable<UserForList> GetAllUsersForList(int id)
        {
            return _usersService.GetAllUsersForList(id);
        }

        [HttpPost]
        public async Task AddAdminRights()
        {
            string result = await Request.Content.ReadAsStringAsync();
            _usersService.AddAdminRights(result);
        }
    }
}

