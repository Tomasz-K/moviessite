﻿using MoviesSite.Business.Dto;
using MoviesSite.Business.Services.Interfaces;
using System.Collections.Generic;
using System.Web.Http;

namespace MoviesSite.WebApi.Controllers
{
    public class MoviesController : ApiController
    {
        private readonly IMoviesService _moviesService;

        public MoviesController(IMoviesService moviesService)
        {
            _moviesService = moviesService;
        }

        [HttpGet]
        public IEnumerable<MovieDto> GetAllMovies()
        {
            return _moviesService.GetAllMovies();
        }

        [HttpGet]
        public MovieDto GetMovieById(int id)
        {
            return _moviesService.GetMovieById(id);
        }

        [HttpPost]
        public void AddMovie(MovieDto movie)
        {
            _moviesService.AddMovie(movie);
        }
    }
}
