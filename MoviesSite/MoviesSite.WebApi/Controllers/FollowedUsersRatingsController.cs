﻿using MoviesSite.Business.Dto;
using MoviesSite.Business.Services;
using System.Collections.Generic;
using System.Web.Http;
using MoviesSite.Business.Services.Interfaces;

namespace MoviesSite.WebApi.Controllers
{
    public class FollowedUsersRatingsController : ApiController
    {
        private readonly IRatingsService _ratingsService;

        public FollowedUsersRatingsController(IRatingsService ratingsService)
        {
            _ratingsService = ratingsService;
        }

        [HttpPost]
        public IEnumerable<RatingDto> GetFollowedUsersThisMovieRatings(LoggedUserIdAndMovieId ids)
        {
            return _ratingsService.GetFollowedUsersThisMovieRatings(ids);
        }
    }
}
