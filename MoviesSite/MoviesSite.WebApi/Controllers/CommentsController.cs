﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Services.Interfaces;

namespace MoviesSite.WebApi.Controllers
{
    public class CommentsController : ApiController
    {
        private readonly ICommentsService _commentsService;

        public CommentsController(ICommentsService commentsService)
        {
            _commentsService = commentsService;
        }

        [HttpGet]
        public IEnumerable<CommentDto> GetAllCommentsByMovieId(int id)
        {
            return _commentsService.GetAllCommentsByMovieId(id);
        }

        [HttpPost]
        public void AddComment(CommentDto commentDto)
        {
           _commentsService.AddComment(commentDto);
        }
    }
}
