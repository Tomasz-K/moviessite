﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using MoviesSite.Business.Dto;
using MoviesSite.Business.Services.Interfaces;

namespace MoviesSite.WebApi.Controllers
{
    public class RegisterController : ApiController
    {
        private readonly IUsersService _usersService;

        public RegisterController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpPost]
        public void AddNewUser(UserDto userDto)
        {
           _usersService.AddNewUser(userDto);
        }
    }
}
