import { Component, OnInit } from '@angular/core';
import {Movie} from '../movie';
import {MoviesService} from '../service/movies.service';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {

  movies: Movie[] = [];

  constructor(private _moviesService: MoviesService) { }

  ngOnInit() {
    this._moviesService.getAllMovies().subscribe(data => this.movies = data);
  }

}
