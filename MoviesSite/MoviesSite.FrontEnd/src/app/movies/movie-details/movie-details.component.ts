import {Component, OnInit} from '@angular/core';
import {MoviesService} from '../service/movies.service';
import {Movie} from '../movie';
import {ActivatedRoute, Router} from '@angular/router';
import {CommentsService} from '../service/comments.service';
import {Comment} from '../comment';
import {UserService} from '../../security/user.service';
import {RatingsService} from '../service/ratings.service';
import {Rating} from '../rating';
import {LoggedUserIdAndMovieId} from '../../security/logged-user-id-and-movie-id';
import {Observable} from 'rxjs/Observable';
import {Director} from '../director';
import {DirectorService} from '../add-director/service/director.service';


@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {

  movie: Movie = new Movie();
  comments: Comment[] = [];
  comment = new Comment();
  movieDirector: Director = new Director();
  rating = new Rating();
  average: number;
  followedUsersRatings$: Observable<Rating[]>;
  loggedUserIdAndMovieId = new LoggedUserIdAndMovieId();

  constructor(private _moviesService: MoviesService,
              private commentService: CommentsService,
              private _route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private ratingService: RatingsService) {
  }

  ngOnInit() {
    const id = this._route.snapshot.paramMap.get('id');
    this._moviesService.getMovieById(id).subscribe(value =>
      { this.movie = value;
        this._moviesService.getMovieDirectorById(value.DirectorId).subscribe(data => this.movieDirector = data); },
      err => this.router.navigateByUrl('/movies'));


    this.commentService.getCommentsByMovieId(id).subscribe(data => this.comments = data,  );
    this._moviesService.getAverageRate(id).subscribe(data => this.average = data);
    this.getFollowedUsersRatings();
  }

  addRating(rate: number) {
    this.rating.MovieId = this._route.snapshot.paramMap.get('id');
    this.rating.Login = this.userService.getCurrentUser().UserLogin;
    this.rating.Rate = rate;
    this.ratingService.addRating(this.rating).subscribe(resp => {
      this.rating = new Rating();
    });
  }

  addComment() {
    this.comment.MovieId = this._route.snapshot.paramMap.get('id');
    this.comment.Login = this.userService.getCurrentUser().UserLogin;
    this.commentService.addComment(this.comment).subscribe(resp => {
      this.comment = new Comment();
      this.router.navigateByUrl('/movies');
    });
  }

  getFollowedUsersRatings() {
    this.loggedUserIdAndMovieId.MovieId = +this._route.snapshot.paramMap.get('id');
    this.loggedUserIdAndMovieId.LoggedUserId = this.userService.getCurrentUser().UserId;
    this.followedUsersRatings$ = this.ratingService.getFollowedUsersRating(this.loggedUserIdAndMovieId);

  }
}
