import { Component, OnInit } from '@angular/core';
import {Director} from '../../director';
import {DirectorService} from '../service/director.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-director',
  templateUrl: './add-director.component.html',
  styleUrls: ['./add-director.component.css']
})
export class AddDirectorComponent implements OnInit {

  director = new Director();

  constructor(private _directorService: DirectorService,
              private _router: Router) { }

  ngOnInit() {
  }

  addNewDirector() {
    this._directorService.postDirector(this.director)
                         .subscribe(resp =>
                         {  this.director = new Director();
                            this._router.navigateByUrl('account')
                         }, err => alert('Director already in the database!'));
  }
}
