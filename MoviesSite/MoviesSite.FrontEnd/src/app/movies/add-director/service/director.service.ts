import { Injectable } from '@angular/core';
import {Director} from '../../director';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable()
export class DirectorService {

  constructor(private _httpClient: HttpClient) { }

  postDirector(director: Director): Observable<Director> {
    return this._httpClient.post<Director>(environment.moviesApi + 'director', director);
  }

  getDirectors(): Observable<Director[]> {
    return this._httpClient.get<Director[]>(environment.moviesApi + 'director');
  }
}
