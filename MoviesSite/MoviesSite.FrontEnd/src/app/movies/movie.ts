import {Director} from './director';
import {Rating} from './rating';
import {Genre} from './genre';

export class Movie {
  Id: number;
  Title: string;
  ProductionYear: Date;
  Description: string;
  GenreId: number;
  DirectorId: number;
  CommentsList: Comment[] = [];
  RatingsList: Rating[] = [];
}
