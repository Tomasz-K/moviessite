import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Movie} from '../movie';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {Genre} from '../genre';
import {Director} from '../director';

@Injectable()
export class MoviesService {

  constructor(private _httpClient: HttpClient) { }

  getAllMovies(): Observable<Movie[]> {
    return this._httpClient.get<Movie[]>(environment.moviesApi + 'movies');
  }

  getMovieById(id: string): Observable<Movie> {
    return this._httpClient.get<Movie>(environment.moviesApi + 'movies/' + id);
  }

  postMovie(movie: Movie): Observable<Movie> {
    return this._httpClient.post<Movie>(environment.moviesApi + 'movies', movie);
  }

  getGenres(): Observable<Genre[]> {
    return this._httpClient.get<Genre[]>(environment.moviesApi + 'genre');
  }

  getAverageRate(id: string): Observable<number> {
    return this._httpClient.get<number>(environment.moviesApi + 'ratings/' + id);
  }
  getMovieDirectorById(id: number): Observable<Director> {
    return this._httpClient.get<Director>(environment.moviesApi + 'director/' + id);
  }
}
