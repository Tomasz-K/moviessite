import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {Rating} from '../rating';
import {LoggedUserIdAndMovieId} from '../../security/logged-user-id-and-movie-id';
import {UserForList} from '../../user-account/user-for-list';

@Injectable()
export class RatingsService {

  constructor(private httpClient: HttpClient) {
  }

  addRating(rating: Rating): Observable<Rating> {
    return this.httpClient.post<Rating>(environment.moviesApi + 'ratings/', rating);
  }


  getFollowedUsersRating(loggedUserIdAndMovieId: LoggedUserIdAndMovieId): Observable<Rating[]> {
    return this.httpClient.post<Rating[]>(environment.moviesApi + 'FollowedUsersRatings', loggedUserIdAndMovieId);

  }

}
