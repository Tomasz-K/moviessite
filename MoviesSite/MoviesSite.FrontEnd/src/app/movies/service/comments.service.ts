import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {Comment} from '../comment';

@Injectable()
export class CommentsService {

  constructor(private httpClient: HttpClient) { }

  getCommentsByMovieId(id: string): Observable<Comment[]> {
    return this.httpClient.get<Comment[]>(environment.moviesApi + 'comments/' + id);
  }

  addComment(comment: Comment): Observable<Comment> {
    return this.httpClient.post<Comment>(environment.moviesApi + 'comments/', comment);
  }
}
