import { Component, OnInit } from '@angular/core';
import {MoviesService} from '../service/movies.service';
import {Router} from '@angular/router';
import {Movie} from '../movie';
import {DirectorService} from '../add-director/service/director.service';
import {Director} from '../director';
import {Genre} from '../genre';

@Component({
  selector: 'app-add-movies',
  templateUrl: './add-movies.component.html',
  styleUrls: ['./add-movies.component.css']
})
export class AddMoviesComponent implements OnInit {

  movie = new Movie();
  directorsList: Director[] = [];
  selectedDirector: Director;

  genresList: Genre[] = [];
  selectedGenre: Genre;

  constructor(private _moviesService: MoviesService,
              private _directorService: DirectorService,
              private _router: Router) { }

  ngOnInit() {
    this._directorService.getDirectors().subscribe(data=>this.directorsList = data);
    this._moviesService.getGenres().subscribe(data => this.genresList = data);
  }

  addNewMovie() {
    this.movie.DirectorId = this.selectedDirector.Id;
    this.movie.GenreId = this.selectedGenre.Id;
    this._moviesService.postMovie(this.movie)
                       .subscribe( resp =>
                       {this.movie = new Movie();
                       this.directorsList = [];
                       this.genresList = [];
                        this._router.navigateByUrl('account');
                       }, err => alert('Movie already in the database!'));
  }
}
