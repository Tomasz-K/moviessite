export class Director {
  Id: number;
  Name: string;
  LastName: string;
  Birthdate: Date;
}
