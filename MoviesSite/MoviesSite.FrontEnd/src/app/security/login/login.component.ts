import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {AuthenticationService} from '../authentication/authentication.service';
import {Credentials} from '../credentials';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  userCredentials: Credentials = new Credentials();

  constructor(private authService: AuthenticationService,
              private userService: UserService,
              private router: Router) {
  }

  login() {
    this.authService.login(this.userCredentials)
      .subscribe(user => {
        if (user.IsAuthenticated) {
          this.userService.setCurrentUser(user);
          this.router.navigateByUrl('/welcome-page');
        } else {
          this.userService.removeUser();
          alert('Incorrect login or password.');
        }
        this.userCredentials = new Credentials();
      });
  }

}
