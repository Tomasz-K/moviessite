import { TestBed, inject } from '@angular/core/testing';

import { AssignAdministratorRightsService } from './assign-administrator-rights.service';

describe('AssignAdministratorRightsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssignAdministratorRightsService]
    });
  });

  it('should be created', inject([AssignAdministratorRightsService], (service: AssignAdministratorRightsService) => {
    expect(service).toBeTruthy();
  }));
});
