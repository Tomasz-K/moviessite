import { Component, OnInit } from '@angular/core';
import {RegisterService} from '../register.service';
import {Credentials} from '../credentials';
import {Router} from '@angular/router';
import {ValidPassword} from './valid-password';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  credentials = new Credentials();
  public validPassword: ValidPassword;

  constructor(private registerService: RegisterService,
              private router: Router) { }

  response: boolean;

  ngOnInit() {
    this.validPassword = {
      password: '',
      confirmPassword: ''
    };
  }
addCredentials() {
    if (this.credentials.Login && this.credentials.Password != null) {
  this.registerService.addCredentials(this.credentials).subscribe(response => {
    this.credentials = new Credentials();
    this.router.navigateByUrl('/login');
    }, err=> alert("User already in the database"))};
}
}
