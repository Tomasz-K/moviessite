export interface ValidPassword {
  password: string;
  confirmPassword: string;
}
