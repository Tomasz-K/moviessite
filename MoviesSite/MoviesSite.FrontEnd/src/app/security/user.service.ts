import {Injectable} from '@angular/core';
import {User} from './user';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../environments/environment';
import {UserForList} from '../user-account/user-for-list';
import {UserToFollow} from './user-to-follow';

@Injectable()
export class UserService {

  userToFollow = new UserToFollow();

  constructor(private http: HttpClient) {
  }

  setCurrentUser(user: User) {
    const userJson = JSON.stringify(user);
    localStorage.setItem('user', userJson);
  }

  getCurrentUser() {
    const userJson = localStorage.getItem('user');
    if (userJson) {
      return JSON.parse(userJson) as User;
    }

    return null;
  }

  removeUser() {
    localStorage.removeItem('user');
  }

  getUsers(userId: number): Observable<UserForList[]> {
    return this.http.get<UserForList[]>(environment.moviesApi + 'users/' + userId);
  }


  followThisUser(userLogin: string) {
    const currentUser = this.getCurrentUser();
    this.userToFollow.LoggedUserId = currentUser.UserId;
    this.userToFollow.UserToFollowLogin = userLogin;
    this.http.post<any>(environment.moviesApi + 'UserRelations', this.userToFollow).subscribe();
  }

}
