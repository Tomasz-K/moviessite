import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../environments/environment';
import {Credentials} from './credentials';

@Injectable()
export class RegisterService {

  constructor(private http: HttpClient) { }

  addCredentials(credentials: Credentials): Observable<Credentials> {
    return this.http.post<Credentials>(environment.moviesApi + 'register/', credentials);
  }

}
