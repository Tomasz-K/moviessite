export interface User {

  UserId: number;
  UserLogin: string;
  IsAuthenticated: boolean;
  IsAdmin: boolean;

}
