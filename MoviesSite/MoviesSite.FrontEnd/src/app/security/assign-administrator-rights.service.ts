import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class AssignAdministratorRightsService {

  constructor(private http: HttpClient) {
  }

  assignAdministratorRightsToUser(user: string) {
    this.http.post<any>(environment.moviesApi + 'users', user).subscribe();
  }

}
