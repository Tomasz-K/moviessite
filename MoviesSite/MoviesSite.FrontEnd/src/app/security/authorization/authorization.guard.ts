import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, Router} from '@angular/router';
import {UserService} from '../user.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthorizationGuard implements CanActivate, CanActivateChild{

  constructor(private userService: UserService,
              private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const currentUser = this.userService.getCurrentUser();
    if (currentUser && currentUser.IsAuthenticated) {
      return true;
    }
    this.router.navigateByUrl('/login');
    return false;
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(childRoute, state);
  }
}
