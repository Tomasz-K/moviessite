import { Injectable } from '@angular/core';
import {UserService} from '../user.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Credentials} from '../credentials';
import {Observable} from 'rxjs/Observable';
import {User} from '../user';
import {environment} from '../../../environments/environment';

@Injectable()
export class AuthenticationService {

  constructor(private userService: UserService,
              private http: HttpClient,
              private router: Router) {
  }

  login(credentials: Credentials): Observable<User> {
    return this.http
      .post<any>(environment.moviesApi + 'authentication', credentials);
  }

  logout() {
    this.userService.removeUser();
    this.router.navigateByUrl('/login');
  }

}
