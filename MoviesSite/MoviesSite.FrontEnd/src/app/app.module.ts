import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {RegisterComponent} from './security/register/register.component';
import {RegisterService} from './security/register.service';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './security/login/login.component';
import {UserService} from './security/user.service';
import {AuthenticationService} from './security/authentication/authentication.service';
import {AppRoutingModule} from './/app-routing.module';
import {WelcomePageComponent} from './welcome-page/welcome-page.component';


import {MoviesListComponent} from './movies/movies-list/movies-list.component';
import {MoviesService} from './movies/service/movies.service';
import {MovieDetailsComponent} from './movies/movie-details/movie-details.component';
import {DirectorService} from './movies/add-director/service/director.service';
import {AddDirectorComponent} from './movies/add-director/component/add-director.component';
import {NavComponent} from './nav/nav.component';
import {AddMoviesComponent} from './movies/add-movie/add-movies.component';
import {UserAccountComponent} from './user-account/user-account.component';
import {AssignAdministratorRightsService} from './security/assign-administrator-rights.service';
import {CommentsService} from './movies/service/comments.service';
import {RatingsService} from './movies/service/ratings.service';
import {StarRatingModule} from 'angular-star-rating';

import { CollapsibleModule } from 'angular2-collapsible';
import {EqualValidator} from './security/register/equal-validator.directive';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    WelcomePageComponent,
    UserAccountComponent,
    AddDirectorComponent,
    MoviesListComponent,

    UserAccountComponent,
    MovieDetailsComponent,
    AddMoviesComponent,
    NavComponent,
    EqualValidator
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CollapsibleModule
  ],
  providers: [RegisterService,
    UserService,
    AuthenticationService,
    MoviesService,
    AssignAdministratorRightsService,
    CommentsService,
    RatingsService,
    DirectorService],

  bootstrap: [AppComponent]
})
export class AppModule {
}
