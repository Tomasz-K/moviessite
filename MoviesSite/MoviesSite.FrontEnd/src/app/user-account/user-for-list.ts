export interface UserForList {

  UserLogin: string;
  IsAdmin: boolean;
  IsFollowed: boolean;
}
