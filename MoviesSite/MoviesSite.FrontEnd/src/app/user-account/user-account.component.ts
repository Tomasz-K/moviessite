import {Component, OnInit} from '@angular/core';
import {UserService} from '../security/user.service';
import {AssignAdministratorRightsService} from '../security/assign-administrator-rights.service';
import {User} from '../security/user';
import {Observable} from 'rxjs/Observable';
import {UserForList} from './user-for-list';


@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.css']
})
export class UserAccountComponent implements OnInit {

  usersForList$: Observable<UserForList[]>;

  constructor(private userService: UserService,
              private addAdminRights: AssignAdministratorRightsService) {

  }


  ngOnInit() {
    this.getUsersList();

  }


  getUsersList() {
    const currentUser = this.userService.getCurrentUser();
    this.usersForList$ = this.userService.getUsers(currentUser.UserId);
  }

  makeAdmin(userLogin: string) {

    this.addAdminRights.assignAdministratorRightsToUser(userLogin);
  }


  followThisUser(userLogin: string) {
    this.userService.followThisUser(userLogin);
  }
}

