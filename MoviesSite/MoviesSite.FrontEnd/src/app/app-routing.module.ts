import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AuthorizationGuard} from './security/authorization/authorization.guard';
import {LoginComponent} from './security/login/login.component';
import {WelcomePageComponent} from './welcome-page/welcome-page.component';
import {MoviesListComponent} from './movies/movies-list/movies-list.component';
import {RegisterComponent} from './security/register/register.component';
import {MovieDetailsComponent} from './movies/movie-details/movie-details.component';
import {UserAccountComponent} from './user-account/user-account.component';
import {AddMoviesComponent} from './movies/add-movie/add-movies.component';
import {AddDirectorComponent} from './movies/add-director/component/add-director.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard],
    children: [
      {path: '', redirectTo: '/welcome-page', pathMatch: 'full'},
      {path: '', redirectTo: '/account', pathMatch: 'full'},
      {path: 'account', component: UserAccountComponent},
    ]
  },
  {path: 'welcome-page', component: WelcomePageComponent},
  {path: 'movies', component: MoviesListComponent},
  {path: 'movie',
    children: [
      {path: ':id', component: MovieDetailsComponent},
    ]
  },
  {path: 'addmovie', component: AddMoviesComponent},
  {path: 'director', component: AddDirectorComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: '**', redirectTo: '/welcome-page'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthorizationGuard]
})
export class AppRoutingModule {
}
